@echo off
compiler\code\vim_clang_next_error\vim_clang_next_error.exe ^
    compiler\build\project\clang\clang_output.log ^
    %ZAH_COMPILATION_ERROR_INDEX%
SET /A ZAH_COMPILATION_ERROR_INDEX += 1
