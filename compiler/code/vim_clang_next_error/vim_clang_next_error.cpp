#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void HexDump (const char * desc, void * addr, int len) 
{
    int i;
    unsigned char buff[17];
    unsigned char * pc = (unsigned char *)addr;

    // Output description if given.
    if (desc != NULL)
        printf("%s:\n", desc);

    // Process every byte in the data.
    for (i = 0; i < len; i++) {
        // Multiple of 16 means new line (with line offset).

        if ((i % 16) == 0) {
            // Just don't print ASCII for the zeroth line.
            if (i != 0)
                printf("  %s\n", buff);

            // Output the offset.
            printf("  %04x ", i);
        }

        // Now the hex code for the specific character.
        printf(" %02x", pc[i]);

        // And store a printable ASCII character for later.
        if ((pc[i] < 0x20) || (pc[i] > 0x7e)) {
            buff[i % 16] = '.';
        } else {
            buff[i % 16] = pc[i];
        }

        buff[(i % 16) + 1] = '\0';
    }

    // Pad out last line if not exactly 16 characters.
    while ((i % 16) != 0) {
        printf("   ");
        i++;
    }

    // And print the final ASCII bit.
    printf("  %s\n", buff);
}

int main (int argc, char * argv[])
{
    if (argc < 3) /*no command specified*/
    {
        fprintf(stderr, "Usage: clang_vim_next_error clang_output.log error_number\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        FILE * fileStream;
        errno_t fileError = fopen_s(&fileStream, argv[1], "r");
        if (fileError != 0) {
            fprintf(stderr, "Couldn't open file\n");
            return 1;
        }

        int requestedErrorIndex = atoi(argv[2]);
        //printf("Requested error index: %d\n", requestedErrorIndex);

        // obtain file size
        fseek(fileStream, 0, SEEK_END);
        long fileSize = ftell(fileStream);
        rewind(fileStream);

        // dump file to memory
        char * file = (char *)calloc(1, sizeof(char)*fileSize);
        fread(file, 1, fileSize, fileStream);
        fclose(fileStream);

        /* An example error with clang-cl.
         *[1mC:\projects\zah\compiler\code\source\zah_parser.cpp(440,18):  [0m[0;1;31merror: [0m[1muse of undeclared identifier 'decl'[0m
         *      TokenToType(&decl->type, typeTok);
         *
         * A clang error has a slightly different format, the numbers are given like Ex. file.cpp:440:18:   ....
         */

        //
        // backslash to forward slash
        //
        for (char * ptr = file; ptr < file + fileSize; ++ptr) {
            if (*ptr == '\\') {
                *ptr = '/';
            }
        }

        //
        // count number of errors
        //
        char * errorLocations[512] = {};
        int errorCount = 0;

        char * lastErrorLoc = file;
        while (true) {
            char * duh = lastErrorLoc;
            lastErrorLoc = strstr(duh, "error: ");
            if (!lastErrorLoc) {
                break;
            }
            errorLocations[errorCount++] = lastErrorLoc;

            // eat forward so we don't get the same result again
            lastErrorLoc++; 
        }

        //printf("Error count: %d", errorCount);

        // loop to first error if requested index is over count
        requestedErrorIndex = requestedErrorIndex % errorCount;

        //
        // Find requested error and extract info
        //
        char * errorLoc = errorLocations[requestedErrorIndex];
        char * errorLineNumberStart = nullptr;
        char * errorColumnNumberStart = nullptr;
        char filename[512] = {};

        // search backwards for line and column number
        for (char * ptr = errorLoc; errorLoc > file; ptr--) {

            if (*ptr == ',' && !errorColumnNumberStart) {
                errorColumnNumberStart = ptr + 1;
            }

            if (*ptr == '(' && !errorLineNumberStart) {
                errorLineNumberStart = ptr + 1;
            }

            if (*ptr == '\n') {
                bool found = false;
                char * localPtr = ptr + 1;
                for (localPtr = ptr + 1; localPtr < file + fileSize; ++localPtr) {

                    // sometimes there's a second escape?
                    if (*localPtr == '\x6D' && *(localPtr + 4) != '\x6D') {
                        localPtr++; // eat past escape character

                        char * filenameStart = localPtr;
                        int filenameLength = errorLineNumberStart - filenameStart - 1;
                        memcpy(&filename[0], filenameStart, filenameLength);
                        found = true;

                        break; // inner loop
                    }
                }

                if (found) {
                    break; // outer loop
                }

                printf("Trouble parsing error\n");
                break;
            }

            if (ptr == file + 1) {
                printf("Trouble parsing error\n");
                break;
            }
        }

        if (!errorLoc || !errorLineNumberStart || !errorColumnNumberStart) {
            fprintf(stderr, "Trouble parsing error\n");
            return 1;
        }

        //
        // Build vim command
        //
        int lineNumber = atoi(errorLineNumberStart);
        int columnNumber = atoi(errorColumnNumberStart);
        char buf[512] = {};
        snprintf(buf, 512, "gvim --remote-silent \"+call cursor(%d, %d)\" %s",
                lineNumber,
                columnNumber,
                &filename[0]);
        
        //puts(buf);

        // call shell
        system(buf);

    }

    return 0;
}
