#include "zah_compiler.h"

namespace zah {

Token Parser::AssertGetNextToken (TokenType type) {
	Token t = tokenizer.GetNextToken();
	if (t.type != type) {
		 AddCompilationError(t, "Unexpected token found.\n    Expected: {}\n    Actual: {}", type, t);
		 return Token();
	}
	else {
		return t;
	}
}

void Parser::AssertCurrentTokenType (TokenType type) {
    Token currentToken = tokenizer.GetCurrentToken();
    if (currentToken.type != type){
        AddCompilationError(currentToken, "Unexpected current token.\n    Expected: {}\n    Actual: {}", type, currentToken);
    }
}

template <class... Args>
void Parser::AddCompilationError (Token offendingToken, StringView msg, Args &&... args) {
    AddCompilationError(offendingToken.location, msg, std::forward<Args>(args)...);
}

template <class... Args>
void Parser::AddCompilationError (Location offendingLocation, StringView msg, Args &&... args) {
    sp::StringView view(msg.str, msg.length);
	char messageBuf[1024] = {};
    int result = sp::format(messageBuf, 1024, view, std::forward<Args>(args)...);
    if (result < 0) {
        CompilerAssert("Buffer wasn't big enough");
    }

    CompilationError * error = &compilationErrors[compilationErrorCount++];
    result = sp::format(&error->message[0], 1024, "Error: {}:{}: {}\n", offendingLocation.file, offendingLocation.lineNumber, messageBuf);
    if (result < 0) {
        CompilerAssert("Buffer wasn't big enough");
    }

    error->offendingLocation = offendingLocation;
}

void * Parser::NewBody (ASTType type, int size) {
	void * bodyMem = ((uint8_t *)tree.data) + tree.usedSize;

	memset(bodyMem, 0, size);

	tree.usedSize += size;

	CompilerAssert(tree.usedSize < tree.allocatedSize, "Requested more buffer than we have");

	return bodyMem;
}

void Parser::InitBody (void * mem, ASTType type, int size) {
	ASTHeader * header = (ASTHeader*)mem;
	header->astType = type;
	header->mySize = size;
}

#define NEW_BODY(ptr, type)						\
    type * ptr;										                \
    ptr = (type *)::zah::Parser::NewBody(AST_##type, sizeof(type)); \
    *ptr = type();									                \
    ::zah::Parser::InitBody(ptr, AST_##type, sizeof(type))

void Parser::AddDeclaredVariable (Type type, Variable * variable) {
	variable->type = type;
	tree.declaredVariables[tree.declaredVariablesCount] = variable;

	tree.declaredVariableCountThisScope[tree.scopeDepth]++;
	tree.declaredVariablesCount++;
}

void Parser::AddDeclaredFunction (FunctionDecl * decl) {
	tree.declaredFunctions[tree.declaredFunctionsCount++] = decl;
}

void Parser::PushVariableStack () {
	tree.scopeDepth++;
}

void Parser::PopVariableStack () {
	int variablesThisScope = tree.declaredVariableCountThisScope[tree.scopeDepth];
	for (int i = 0; i < variablesThisScope; ++i) {
		tree.declaredVariables[--tree.declaredVariablesCount] = {};
	}
	tree.declaredVariableCountThisScope[tree.scopeDepth] = 0;
	tree.scopeDepth--;
}

void Parser::AssertTokenType (Token token, TokenType type) {
    if (type != token.type) {
        AddCompilationError(token, "Unexpected token type\n    Expected: {}\n    Actual: {}", type, token);
    }
}

enum ConstantTypesEnum {
	ConstantTypes_String,

	ConstantTypes_Bool,
	ConstantTypes_GenericInteger,
	ConstantTypes_Int,
	ConstantTypes_Short,
	ConstantTypes_Long,

	ConstantTypes_GenericFloat,
	ConstantTypes_Float,
	ConstantTypes_Double,
};

const char * ConstantsTypes[] = {
	"char *",

	"bool",
	"g_integer",
	"int",
	"short",
	"long",

	"g_float",
	"float",
	"double"
};

static bool IsAnyIntegerType (Type a) {
	if (StrEq(a.name, ConstantsTypes[ConstantTypes_Int]) ||
		StrEq(a.name, ConstantsTypes[ConstantTypes_Short]) ||
		StrEq(a.name, ConstantsTypes[ConstantTypes_Long])) {
		return true;
	}
	return false;
}

void Parser::CheckAssignmentTypeSafety (Type a, Type b, Location location) {
	if (StrEq(a.name, b.name)) {
        // pass!
	}
	else if (StrEq(b.name, StringView("g_integer"))) {
		if (!IsAnyIntegerType(a)) {
			AddCompilationError(location, "type mismatch: not an integer");
		}
	}
	else if (StrEq(a.name, StringView("g_integer"))) {
		if (!IsAnyIntegerType(b)) {
			AddCompilationError(location, "type mismatch: not an integer");
		}
	}
	else {
		AddCompilationError(location, "assignment type mismatch: lhs - {}, rhs - {}", a.name, b.name);
	}
}

Type Parser::GetFunctionCallType (FunctionCall * call) {
	for (int i = 0; i < tree.declaredFunctionsCount; ++i) {
		if (StrEq(call->callee, tree.declaredFunctions[i]->name)) {
			return tree.declaredFunctions[i]->returnType;
		}
	}

	AddCompilationError(Location(), "undeclared function");
	return Type();
}

Type Parser::GetArgumentType (ASTHeader * header) {
	switch (header->astType) {
		case AST_StringConstant: {
			Type type = {};
			type.name = StringView(ConstantsTypes[ConstantTypes_String]);
			return type;
		}
		case AST_NumberConstant: {
			Type type = {};
			type.name = StringView(ConstantsTypes[ConstantTypes_GenericInteger]);
			return type;
		}
		case AST_Variable: {
			Variable * var = (Variable *)header;
			bool found = false;
			for (int i = 0; i < tree.declaredVariablesCount; ++i) {
				if (StrEq(var->name, tree.declaredVariables[i]->name)) {
					var->type = tree.declaredVariables[i]->type;
					found = true;
					break;
				}
			}

			if (!found) {
				AddCompilationError(Location(), "undeclared variable"); // TODO(james): get a real location
			}

			return var->type;
		}
		case AST_FunctionCall: {
			FunctionCall * call = (FunctionCall *)header;
			return GetFunctionCallType(call);
		}
		case AST_MathExpr: {
			MathExpr * mathExpr = (MathExpr *)header;
			return mathExpr->type;
		}
		case AST_ParenExpr: {
			ParenExpr * parenExpr = (ParenExpr *)header;
			return GetArgumentType(parenExpr->expr);
		}
		case AST_PrefixExpr: {
			PrefixExpr * prefixExpr = (PrefixExpr *)header;
			return prefixExpr->type;
		}
		default: {
			CompilerAssert("not implemented: find argument type");
			return Type();
		}
	}
}

ASTHeader * Parser::ParsePreprocessor () {
	Token ppIDToken = tokenizer.GetNextToken();
	if (ppIDToken.type == Tok_Identifier) {
		if (StrEq(ppIDToken.content, "include")) {

			Token stringOrCaret = tokenizer.GetNextToken();
			if (stringOrCaret.type == Tok_LT) {
				NEW_BODY(include, IncludePP);
				include->carets = true;
				Token fileID = tokenizer.GetNextToken();
				include->file = fileID.content;

				AssertGetNextToken(Tok_GT);
				return include;
			}
			else if (stringOrCaret.type == Tok_String) {
				NEW_BODY(include, IncludePP);
				include->carets = false;
				include->file = stringOrCaret.content;
				return include;
			}
			else {
				AddCompilationError(stringOrCaret, "Invalid token after '#include'");
			}
		}
		else {
			AddCompilationError(ppIDToken, "Unknown preprocessor directive");
		}
	}
	else {
		AddCompilationError(ppIDToken, "Invalid preprocessor");
	}

	return nullptr;
}

static Type TokenToType (Token token) {
    Type type = {};
	type.name = token.content;
    return type;
}

static StringConstant TokenToStringConstant (Token token) {
    StringConstant constant = {};
	constant.content = token.content;
    return constant;
}

static NumberConstant TokenToNumberConstant (Token token) {
    NumberConstant constant = {};
	constant.val = atof(CStr(token.content));
    return constant;
}

ASTHeader * Parser::ParseMathExpr (ASTHeader * lhs, Token mathToken) {
	switch (mathToken.type) {

        case Tok_Plus:
        case Tok_Minus:
        case Tok_Divide:
        case Tok_Star: {
			NEW_BODY(add, MathExpr);

			add->lhs = lhs;
			add->rhs = ParseArgument(tokenizer.GetNextToken());

			CheckAssignmentTypeSafety(
                GetArgumentType(add->lhs), 
                GetArgumentType(add->rhs),
                mathToken.location
            );

			// TODO(james): kinda dumb
			add->type = GetArgumentType(lhs);
			add->op = mathToken;

			return add;
		}

        case Tok_CompEqual:
        case Tok_CompNotEqual:
        case Tok_LogicalOr:
        case Tok_LogicalAnd: {
			NEW_BODY(comp, MathExpr);

			comp->lhs = lhs;
			comp->rhs = ParseArgument(tokenizer.GetNextToken());

			CheckAssignmentTypeSafety(
                GetArgumentType(comp->lhs), 
                GetArgumentType(comp->rhs),
                mathToken.location
            );

			comp->type.name = "bool";
			comp->op = mathToken;

			return comp;
		}

        case Tok_Identifier: {
			NEW_BODY(add, MathExpr);

			add->lhs = lhs;
			add->rhs = ParseArgument(mathToken);

			CheckAssignmentTypeSafety(
                GetArgumentType(add->lhs), 
                GetArgumentType(add->rhs),
                mathToken.location
            );

			// TODO(james): kinda dumb
			add->type = GetArgumentType(lhs);
			add->op = mathToken;

			return add;
		}

        case Tok_True:
        case Tok_False: {
			NEW_BODY(add, MathExpr);

			add->lhs = lhs;
			add->rhs = ParseArgument(mathToken);

			Type boolType = {};
			boolType.name = ConstantsTypes[ConstantTypes_Bool];

			CheckAssignmentTypeSafety(GetArgumentType(add->lhs), boolType, mathToken.location);

			// TODO(james): kinda dumb
			add->type = GetArgumentType(lhs);
			add->op = mathToken;

			return add;		
		}
        default: {
			AddCompilationError(mathToken, "invalid math operator");
			return nullptr;
		}
	}
}

ASTHeader * Parser::ParsePrefixExpr (Token arg) {
	NEW_BODY(prefixExpr, PrefixExpr);
	switch (arg.type) {
		case Tok_Negate: {
			prefixExpr->type.name = StringView("bool");
			prefixExpr->op = arg;
			break;
		}

		default: {
			AddCompilationError(arg, "Unknown prefix");
			break;
		}
	}

	return prefixExpr;
}

// some different possible arguments... (var, 5, "Literal", var + 1, SomeFunc())
// might also be right-hand of an assignment. var x = SomeFunc();
ASTHeader * Parser::ParseArgument (Token arg) {
	Token next = tokenizer.GetNextToken();

	switch (arg.type) {
		case Tok_Identifier: {
			if (next.type == Tok_Comma || next.type == Tok_CloseParen || next.type == Tok_Semicolon) {
				NEW_BODY(variable, Variable);
                variable->name = arg.content;
				return (ASTHeader*)variable;
			}
			else if (next.type == Tok_OpenParen) {
				ASTHeader* functionCall = ParseFunctionCall(arg);
				return functionCall;
			} 
			else if (IsMathOperator(next)) {
				NEW_BODY(variable, Variable);
                variable->name = arg.content;
				ASTHeader * header = ParseMathExpr(variable, next);
				return header;
			}
			else {
				AddCompilationError(arg, "Was expecting a comma");
				return nullptr;
			}
			break;
		}
		case Tok_Number: {
			NEW_BODY(numberConstant, NumberConstant);
			*numberConstant = TokenToNumberConstant(arg);
			if (next.type == Tok_Comma || next.type == Tok_CloseParen || next.type == Tok_Semicolon) {
				return (ASTHeader *)numberConstant;
			}
			else if (IsMathOperator(next)) {
				ASTHeader * header = ParseMathExpr(numberConstant, next);
				return header;
			}
			else {
				AddCompilationError(next, "Was expecting a comma");
				return nullptr;
			}
			break;
		}
		case Tok_String: {
			if (next.type == Tok_Comma || next.type == Tok_CloseParen || next.type == Tok_Semicolon) {
				NEW_BODY(stringConstant, StringConstant);
				*stringConstant = TokenToStringConstant(arg);
				return stringConstant;
			}
			else {
				AddCompilationError(next, "Invalid token in argument");
				return nullptr;
			}
			break;
		}
		case Tok_OpenParen: {
			NEW_BODY(parenExpr, ParenExpr);
			ASTHeader * arg = ParseArgument(next);
			parenExpr->expr = arg;

			next = tokenizer.GetNextToken();
			if (next.type == Tok_CloseParen || next.type == Tok_Semicolon) {
				return parenExpr;
			}
			else if (IsMathOperator(next)) {
				ASTHeader * mathExpr = ParseMathExpr(parenExpr, next);
				return mathExpr;
			}
			else {
				AddCompilationError(next, "Unexpected value following paren scope");
			}
			break;
		}
		case Tok_Negate: {
			ASTHeader * prefix = ParsePrefixExpr(arg);
			ASTHeader * header = ParseMathExpr(prefix, next);
			return header;
		}
		case Tok_True: {
			NEW_BODY(variable, NumberConstant);
			variable->val = 1;
			return (ASTHeader *)variable;
		}
		case Tok_False: {
			NEW_BODY(variable, NumberConstant);
			variable->val = 0;
			return (ASTHeader *)variable;
		}
		default: {
			AddCompilationError(arg, "Invalid argument. Arg: {}", arg);
			return nullptr;
		}
	}

    return nullptr;
}

// x = 0;
ASTHeader * Parser::ParseVariableAssignment (Token identifierTok) {
	NEW_BODY(assignment, VariableAssignment);
	
	// lhs
    assignment->variable.name = identifierTok.content;
	
	// rhs
	assignment->rhs = ParseArgument(tokenizer.GetNextToken());

	CheckAssignmentTypeSafety(
        GetArgumentType(&assignment->variable), 
        GetArgumentType(assignment->rhs),
        tokenizer.GetCurrentToken().location
    );

	return assignment;
}

// int x; OR int x = 0;
ASTHeader * Parser::ParseVariableDeclaration (Token typeTok, Token identifierTok) {
    NEW_BODY(declStatement, VariableDeclarationStatement);
    
    // lhs
    declStatement->type = TokenToType(typeTok);
    declStatement->variable.name = identifierTok.content;		

    AddDeclaredVariable(declStatement->type, &declStatement->variable);

	Token semicolonOrEquals = tokenizer.GetNextToken();
	if (semicolonOrEquals.type == Tok_Equals) { // int x = 0;

		NEW_BODY(declExpression, VariableDeclarationExpression);

        declExpression->statement = declStatement;

		// rhs
		declExpression->rhs = ParseArgument(tokenizer.GetNextToken());

		Token currentToken = tokenizer.GetCurrentToken();
		if (currentToken.type != Tok_Semicolon) {
			AssertGetNextToken(Tok_Semicolon);
		}

		CheckAssignmentTypeSafety(
            declStatement->type, 
            GetArgumentType(declExpression->rhs),
            semicolonOrEquals.location
        );

		return declExpression;
	}
	else if (semicolonOrEquals.type == Tok_Semicolon) { // int x;
		return declStatement;
	}
	else {
		AddCompilationError(semicolonOrEquals, "Invalid token following variable declaration");
		return nullptr;
	}
}

ASTHeader * Parser::ParseFunctionCall (Token functionId) {
	NEW_BODY(call, FunctionCall);
	AssertTokenType(functionId, Tok_Identifier);
	call->callee = functionId.content;

	if (tokenizer.GetCurrentToken().type != Tok_OpenParen)
		AssertGetNextToken(Tok_OpenParen);

	tokenizer.GetNextToken();
	while (tokenizer.GetCurrentToken().type != Tok_CloseParen) {
		ASTHeader * arg = ParseArgument(tokenizer.GetCurrentToken());
		call->args[call->argsCount++] = arg;
		CompilerAssert(call->argsCount < 32, "More args than static array size");
	}

	return call;
}

// some statements...
// func(hey);
// int x = 0;
// x = 0;
// x = 1 + 5;
ASTHeader * Parser::ParseStatement () {
	Token current = tokenizer.GetCurrentToken();
	Token next = tokenizer.GetNextToken();
	if (next.type == Tok_OpenParen) {
		// function call
		ASTHeader* functionCall = ParseFunctionCall(current);

		Token semicolon = tokenizer.GetNextToken();
		if (semicolon.type != Tok_Semicolon) {
			AddCompilationError(semicolon, "Invalid end of statement, expecting semicolon. Instead found {}", semicolon);
		}

		return functionCall;
	}
	else if (next.type == Tok_Equals) {
		return ParseVariableAssignment(current);
	}
	else if (next.type == Tok_Identifier) {
		// declaration: current next  ?
		//				int		x	(;/=)
		return ParseVariableDeclaration(current, next);
	}
	else {
		AddCompilationError(next, "invalid statement");
		return nullptr;
	}
}

// current token will be one after last brace
IfStatement * Parser::ParseIfStatement () {
	NEW_BODY(ifStatement, IfStatement);
	AssertGetNextToken(Tok_OpenParen);
	ifStatement->testExpr = ParseArgument(tokenizer.GetNextToken());
	AssertGetNextToken(Tok_OpenBrace);
	ifStatement->body = ParseStatementSequence();
	AssertCurrentTokenType(Tok_CloseBrace);

	Token token = tokenizer.GetNextToken();
	if (token.type == Tok_Else) {
		token = tokenizer.GetNextToken();
		if (token.type == Tok_If) {
			ifStatement->elifStatement = ParseIfStatement();
		}
		else if (token.type == Tok_OpenBrace) {
			// final else
			ifStatement->elseSeq = ParseStatementSequence();
			tokenizer.GetNextToken(); // eat forward
		}
		else {
			AddCompilationError(token, "Unexpected token after if statement");
        }
	}

	return ifStatement;
}

StatementSequence * Parser::ParseStatementSequence () {
	PushVariableStack();

	NEW_BODY(seq, StatementSequence);
	Token token = tokenizer.GetNextToken();
	if (token.type == Tok_CloseBrace) {
		// emit warning: expected a "body", got an empty scope
	}

	while (token.type != Tok_CloseBrace) {
		if (token.type == Tok_Identifier) {
			ASTHeader * statement = ParseStatement();
			seq->statements[seq->statementCount++] = statement;
			CompilerAssert(seq->statementCount < 128, "overflow static array");
			token = tokenizer.GetNextToken();
		}
		else if (token.type == Tok_OpenBrace) {
			ASTHeader * statement = ParseStatementSequence();
			seq->statements[seq->statementCount++] = statement;
			CompilerAssert(seq->statementCount < 128, "overflow static array");
			token = tokenizer.GetNextToken();
		}
		else if (token.type == Tok_Hash) {
			ParsePreprocessor();
			token = tokenizer.GetNextToken();
		}
		else if (token.type == Tok_Return) {
			NEW_BODY(returnStatement, ReturnStatement);
			returnStatement->rhs = ParseArgument(tokenizer.GetNextToken());

			seq->statements[seq->statementCount++] = returnStatement;
			CompilerAssert(seq->statementCount < 128, "overflow static array");
			token = tokenizer.GetNextToken();
		}
		else if (token.type == Tok_If) {
			IfStatement * ifStatement = ParseIfStatement();
			seq->statements[seq->statementCount++] = ifStatement;
			token = tokenizer.GetCurrentToken();
		}
		else {
			AddCompilationError(token, "Invalid token in statement sequence");
		}
	}

	PopVariableStack();

	return seq;
}


ASTHeader * Parser::ParseFunction () {
	AssertCurrentTokenType(Tok_Identifier);
    Token returnType = tokenizer.GetCurrentToken();
	Token functionName = AssertGetNextToken(Tok_Identifier);

	AssertGetNextToken(Tok_OpenParen);

	Token argsToken = tokenizer.GetNextToken();
	while (argsToken.type != Tok_CloseParen) {
		argsToken = tokenizer.GetNextToken();
	}

	Token defOrDecl = tokenizer.GetNextToken();
	if (defOrDecl.type == Tok_Semicolon) {
		NEW_BODY(decl, FunctionDecl);
		// TODO Get arguments
		decl->returnType = TokenToType(returnType);
		decl->name = functionName.content;

		return decl;
	}
	else if (defOrDecl.type == Tok_OpenBrace) {
		NEW_BODY(decl, FunctionDecl);
		NEW_BODY(def, FunctionDef);
		// TODO Get arguments
		decl->returnType = TokenToType(returnType);
		decl->name = functionName.content;

		def->decl = decl;
		def->body = ParseStatementSequence();

		if (!StrEq(decl->returnType.name, StringView("void"))) {
			bool hasReturn = false;
			for (int i = 0; i < def->body->statementCount; ++i) {
				if (def->body->statements[i]->astType == AST_ReturnStatement) {
					ReturnStatement * returnStatement = (ReturnStatement *)def->body->statements[i];
					CheckAssignmentTypeSafety(
                        decl->returnType, 
                        GetArgumentType(returnStatement->rhs),
                        returnType.location
                    );
					hasReturn = true;
				}
			}

            if (!hasReturn) {
                AddCompilationError(Location(), "Function doesn't return a value"); // TODO(james): get legit location
            }
		}
		else {
			for (int i = 0; i < def->body->statementCount; ++i) {
				if (def->body->statements[i]->astType == AST_ReturnStatement) {
					AddCompilationError(Location(), "Return statement in function that returns void"); // TODO(james): get legit location
				}
			}
		}

		AddDeclaredFunction(decl);
		return def;
	}
	else {
		AddCompilationError(defOrDecl, "Bad token after function declaration");
	}

	return nullptr;
}

void PrintASTType (ASTType type) {
	Print("{}\n", AstTypeStrings[type]);
}

void Parser::ParseZahFile () {
	bool parsing = true;
	tokenizer.GetNextToken();
	while (parsing) {
		Token currentToken = tokenizer.GetCurrentToken();
		switch (currentToken.type) {
			case Tok_Eof: {
				parsing = false;
				break;
			}

			case Tok_Identifier: {
				ASTHeader * header = ParseFunction();
				tokenizer.GetNextToken();
				tree.topLevelEntries[tree.topLevelEntriesCount++] = header;
				break;
			}

			case Tok_Hash: {
				ASTHeader * header = ParsePreprocessor();
				tokenizer.GetNextToken();
				tree.topLevelEntries[tree.topLevelEntriesCount++] = header;
				break;
			}

			default: {
				AddCompilationError(currentToken, "Unexpected top-level token."); 
				break;
			}
		}
	}

	//
	// Print AST tree, checks size validities
	//
	uint8_t * treePos = (uint8_t *)tree.data;
	uint8_t * treeEnd = (uint8_t *)tree.data + tree.usedSize;
	while (treePos < treeEnd) {
		ASTHeader * header = (ASTHeader *)treePos;
		
		PrintASTType(header->astType);
		
		treePos += header->mySize;
	}

	Print("End of AST, all entries. \n\n");
}

#if 0


bool TypesEqual(Type a, Type b) {
	if (StrEq(a.content, b.content)) {
		return true;
	}
	else {
		return false;
	}
}




StringView GetType (ASTHeader * header) {
	StringView view;
	switch (header->type) {
		case AST_NumberConstant: {
			view.str = &g_builtin_types[1];
			view.length = 3;
			return view;
		}
		case AST_StringConstant: {
			view.str = &g_builtin_types[2];
			view.length = 6;
			return view;
		}
		default: {
			ErrorAssert("Type checking error, unhandled type"); 
			break;
		}
	}
}

struct DeclaredVariables {
	Type type;
	Variable variable;
};

StringView GetVariableType(Variable var, DeclaredVariables* vars, int declCount) {
	for (int i = 0; i < declCount; ++i) {
		if (StrEq(var.content, vars.content[i])) {
		
		}
	}
}

void TypeChecking () {
	DeclaredVariables vars[128] = {};
	int declCount = 0;

	uint8_t* treePos = (uint8_t*)tree.data;
	uint8_t* treeEnd = (uint8_t*)tree.data + tree.usedSize;
	while (treePos < treeEnd && !g_compiliationError) {
		ASTHeader* header = (ASTHeader*)treePos;

		switch (header->type) {
			if (header->type == AST_VariableAssignment) {
				VariableAssignment* assign = (VariableAssignment*)treePos;
				StrEq(GetVariableType(assign->variable), GetType(assign->rhs));
			}
		
		}

		

		treePos += header->mySize;
	}
}

void CheckProgram() {
	
	TypeChecking();
}

#endif

} // namespace zah
