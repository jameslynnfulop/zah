#include "zah_compiler.h"
#include "zah_driver.cpp"

namespace zah {

enum CLTokenType
{
	CLToken_Dash,
	CLToken_String
};

struct CLToken {
	char * str;
	int length;
	CLTokenType type;
};

struct CLTokenList {
	CLToken tokens[64];
	int count;
};

static void CLAddToken (CLTokenList * tokenList, CLTokenType type, char * str, int tokenLength) {
	tokenList->tokens[tokenList->count].type = type;
	tokenList->tokens[tokenList->count].str = str;
	tokenList->tokens[tokenList->count].length = tokenLength;

	++tokenList->count;

	CompilerAssert(tokenList->count < 64, "Overflow static array");
}

static int CompileCommandLine (int argCount, char *args[]) {
	//
	// PARSE COMMAND LINE
	//

	// Arg 0 is the name of the program?
	if (argCount == 1) {
		CompilerAssert("Zah Compiler: pass some arguments");
		return 0;
	}

	CLTokenList tokens = {};
	for (int argIndex = 1; argIndex < argCount; ++argIndex) {
		char * arg = args[argIndex];

        char * cPtr = arg;
		while (*cPtr != '\0') {
			char c = *cPtr;

			if (c != ' ') {
				int length = 0;
				char * start = cPtr;
				c = *cPtr;
				while (c != ' ' && c != '\0') {
					length++;
					c = cPtr[length];
				}
				cPtr += length - 1;
				CLAddToken(&tokens, CLToken_String, start, length);
			}

			cPtr++;
		}
	}


	if (tokens.count == 0) {
		CompilerAssert("Failed to tokenize passed in argument!");
		return 0;
	}

#if 0
	printf("Tokens:\n");
	for (int i = 0; i < tokens.count; i++) {
		switch (tokens.tokens[i].type) {
		case Token_Dash: {
			printf("Dash\n");
			break;
		}
		case Token_String: {
			printf("String\n");
			break;
		}
		}
	}
#endif

	CLToken fileIn = tokens.tokens[0];
	char * fileInPath = (char*)calloc(1, fileIn.length + 1);
	memcpy(fileInPath, fileIn.str, fileIn.length);

	CLToken fileOut = tokens.tokens[1];
	char * fileOutPath = (char*)calloc(1, fileOut.length + 1);
	memcpy(fileOutPath, fileOut.str, fileOut.length);

	Error error;
	CompileZahFileToFile(fileInPath, fileOutPath, &error);
	if (error.Ok()) {
		Print("\nSuccess!");
	}
	else {
		Print("Compilation Errors.");
	}

	getchar();

	return 1;
}

} // namespace zah

// entry point
int main (int argCount, char *args[]) {
    return zah::CompileCommandLine(argCount, args);
}
