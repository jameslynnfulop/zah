#pragma once

#include <iostream>  
#include <vector>

#include "sp.h"

namespace zah {

struct StringView;

bool StrEq (StringView a, StringView b);

int32_t StringLength (const char * str) {
	return int32_t(strlen(str));
}

struct StringView {
	const char * str;
	int32_t length;

	StringView () : str(nullptr), length(0) {};
	StringView (const char * lit) {
		str = lit;
		length = StringLength(lit);
	}

	StringView (const char * lit, int len) {
		str = lit;
		length = len;
	}


    bool operator == (const StringView & b) {
        if (StrEq(*this, b)) {
            return true;
        }

        return false;
    }

    bool operator != (const StringView & b) {
        return !(*this == b);
    }
};

bool StrEq (StringView a, StringView b) {
	if (a.length == b.length) {
		for (int i = 0; i < a.length; ++i) {
			if (a.str[i] != b.str[i]) {
				return false;
            }
		}
	}
	else {
		return false;
	}

	return true;
}

struct Location {
	StringView file = StringView();
	uint16_t lineNumber = 0;
	uint16_t horizontalPosition = 0;
};

struct Error {
	StringView message = StringView();
	Location location;

	void Update (StringView msg, Location loc) {
		message = msg;
		location = loc;
	}
	
	bool Ok () {
		return message.length ==  0;
	}
};

#define ERROR_RETURN(err, val)if (!err.Ok()) return val; 
#define ERROR_CHECK(cond, err, msg, val) \
    if (!(cond)) { \
	    err.Update(msg, Location(__FILE_NAME__, __FILE_NUMBER__)); \
		return val; \
	}
#define ERROR_UPDATE(err, msg)err.Update(msg, Location(__FILE_NAME__, __FILE_NUMBER__));

#define ENUM_SURROUND(a)a,

#define TOKEN_TYPES					\
	ENUM_SURROUND(Tok_OpenParen)	\
	ENUM_SURROUND(Tok_CloseParen)	\
	ENUM_SURROUND(Tok_OpenBrace)	\
	ENUM_SURROUND(Tok_CloseBrace)	\
	ENUM_SURROUND(Tok_OpenBracket)	\
	ENUM_SURROUND(Tok_CloseBracket)	\
	ENUM_SURROUND(Tok_String)		\
	ENUM_SURROUND(Tok_Identifier)	\
	ENUM_SURROUND(Tok_Number)		\
	ENUM_SURROUND(Tok_Divide)		\
	ENUM_SURROUND(Tok_Plus)			\
	ENUM_SURROUND(Tok_Minus)		\
	ENUM_SURROUND(Tok_Equals)		\
	ENUM_SURROUND(Tok_Star)			\
	ENUM_SURROUND(Tok_Eof)			\
	ENUM_SURROUND(Tok_Semicolon)	\
	ENUM_SURROUND(Tok_Hash)			\
	ENUM_SURROUND(Tok_LT)			\
	ENUM_SURROUND(Tok_GT)			\
	ENUM_SURROUND(Tok_Period)		\
	ENUM_SURROUND(Tok_Comma)		\
	ENUM_SURROUND(Tok_Return)		\
	ENUM_SURROUND(Tok_If)			\
	ENUM_SURROUND(Tok_Else)			\
	ENUM_SURROUND(Tok_Case)			\
	ENUM_SURROUND(Tok_Switch)		\
	ENUM_SURROUND(Tok_CompEqual)	\
	ENUM_SURROUND(Tok_CompNotEqual)	\
	ENUM_SURROUND(Tok_LogicalAnd)	\
	ENUM_SURROUND(Tok_BitwiseAnd)	\
	ENUM_SURROUND(Tok_LogicalOr)	\
	ENUM_SURROUND(Tok_BitwiseOr)	\
	ENUM_SURROUND(Tok_Negate)		\
	ENUM_SURROUND(Tok_Negative)		\
	ENUM_SURROUND(Tok_True)			\
	ENUM_SURROUND(Tok_False)		\
	ENUM_SURROUND(Tok_Unknown)		\
	ENUM_SURROUND(Tok_Invalid)

#define AST_TYPES 						\
	ENUM_SURROUND(AST_IncludePP)		\
	ENUM_SURROUND(AST_ZahError)			\
	ENUM_SURROUND(AST_NumberConstant)	\
	ENUM_SURROUND(AST_StringConstant)	\
	ENUM_SURROUND(AST_Type)				\
	ENUM_SURROUND(AST_FunctionDecl)		\
	ENUM_SURROUND(AST_Variable)			\
	ENUM_SURROUND(AST_FunctionDef)		\
	ENUM_SURROUND(AST_FunctionCall)		\
	ENUM_SURROUND(AST_ReturnStatement) 	\
	ENUM_SURROUND(AST_VariableAssignment)\
	ENUM_SURROUND(AST_VariableDeclarationExpression)\
	ENUM_SURROUND(AST_VariableDeclarationStatement)\
	ENUM_SURROUND(AST_StatementSequence)\
	ENUM_SURROUND(AST_MathExpr)			\
	ENUM_SURROUND(AST_ParenExpr)		\
	ENUM_SURROUND(AST_PrefixExpr)		\
	ENUM_SURROUND(AST_IfStatement)		\
	ENUM_SURROUND(AST_Invalid)

enum TokenType {
	TOKEN_TYPES
};

enum ASTType {
	AST_TYPES
};

#undef  ENUM_SURROUND
#define ENUM_SURROUND(a)#a,

const char * TokenTypeStrings[] = {
	TOKEN_TYPES
};

const char * AstTypeStrings[] = {
	AST_TYPES
};

/*
enum class CompilationErrorCode {
    Err_UnexpectedToken, // malformed sequence.
    Err_TypeMismatch,    // detected bad type. Ex. returning value in void function
    Err_UndeclaredIdentifier, // trying to use an identifer that hasn't been declared
    Err_InvalidSyntax   // syntax we can't parse. Ex. #include "ab.cpp"!!!;
}
*/


//
// AST
//
struct Token {
	StringView content = {};
	TokenType type = Tok_Invalid;

    Location location = {};

    bool operator == (const Token & b) {
        if (content == b.content && type == b.type) {
            return true;
        }

        return false;
    }

    bool operator != (const Token & b) {
        return !(*this == b);
    }
};

class Tokenizer {

public:
    Tokenizer (const char * fileStart, const char * filePath)
        : ptr(fileStart)
        , file(filePath)
    {}

    Token PeekNextToken ();
    Token GetNextToken ();

    Token GetCurrentToken () const;

private:

    void IncrementSourceLineNumber ();

	const char * ptr = nullptr;
	const char * nextTokenPtr = nullptr;
	
	Token currentToken = {};
	Token nextToken = {};
	
	const char * file = nullptr;
	int lineNumber = -1;

	int nextTokenLineNumber = -1;
};

struct CompilationError {
    char message[1024] = {};
    Location offendingLocation = {};
};

struct ASTHeader {
	ASTHeader () {
		astType = AST_Invalid;
		mySize = 0;
	}
	ASTType astType; // must be first!

	int mySize;
};

#define ASTHEADER_CTOR(type)astType = AST_##type; mySize = sizeof(type);

#define ASTHEADER_CTOR_REV(type)type() {                                    \
    astType = AST_##type;                                                   \
    mySize = sizeof(type);                                                  \
}                                                                           \

// pre-processor. Ex. #include <stuff.h>
struct IncludePP : ASTHeader {
    ASTHEADER_CTOR_REV(IncludePP);

	StringView file = {};
	bool carets = false;
};

struct ZahError : ASTHeader {
    ASTHEADER_CTOR_REV(ZahError);
};

// literals like 1, 5
struct NumberConstant : ASTHeader {
    ASTHEADER_CTOR_REV(NumberConstant);
	double val = -1;
};

// void, int, MyClass
struct Type : ASTHeader {
    ASTHEADER_CTOR_REV(Type);
	StringView name = {};
};

// stuff
struct Variable : ASTHeader {
    ASTHEADER_CTOR_REV(Variable);

	Type type = {}; // don't print this
	StringView name = {};
};

struct StringConstant : ASTHeader {
    ASTHEADER_CTOR_REV(StringConstant);
	StringView content = {};
};

// void MyFunc (int a, int b)
struct FunctionDecl : ASTHeader {
    ASTHEADER_CTOR_REV(FunctionDecl);

	StringView name = {};
	Type returnType = Type();

	ASTHeader* args[64]; // should alternate type, var, type, var...
	int argsCount = 0;
};

// bag of statements. 
struct StatementSequence : ASTHeader {
    ASTHEADER_CTOR_REV(StatementSequence);

	ASTHeader* statements[128] = {};
	int statementCount = 0;
};

// void MyFunc(int a, int b) { DoIt(); }
struct FunctionDef : ASTHeader {
    ASTHEADER_CTOR_REV(FunctionDef);

	FunctionDecl * decl = nullptr;
	StatementSequence * body = nullptr;
};

// MyFunc(a, b);
// WrapperFunc(FunctionCallA(), FunctionCallB())
struct FunctionCall : ASTHeader {
    ASTHEADER_CTOR_REV(FunctionCall);

	StringView callee = {};
	ASTHeader* args[32];
	int argsCount = 0;
};


// int x;
struct VariableDeclarationStatement : ASTHeader {
    ASTHEADER_CTOR_REV(VariableDeclarationStatement);

	Type type = Type();
	Variable variable = Variable();
};

/*
Expressions have a 'return value.' An expression doesn't yield a new value, but it may produce side effects
*/

// int x = 0;   int x = 2+3;   int x = GetNum();
struct VariableDeclarationExpression : ASTHeader {
    ASTHEADER_CTOR_REV(VariableDeclarationExpression);

    VariableDeclarationStatement * statement = nullptr;
	ASTHeader* rhs = nullptr;
};

// x = 0;
struct VariableAssignment : ASTHeader {
    ASTHEADER_CTOR_REV(VariableAssignment);

	Variable variable = {};
	ASTHeader* rhs = nullptr;
};

struct ReturnStatement : ASTHeader {
    ASTHEADER_CTOR_REV(ReturnStatement);

	ASTHeader * rhs = nullptr;
};

//
// MATH
//

struct MathExpr : ASTHeader {
    ASTHEADER_CTOR_REV(MathExpr);

	ASTHeader * lhs = nullptr;
	ASTHeader * rhs = nullptr;

	Type type = Type(); // int, bool...
	Token op = Token();
};

struct ParenExpr : ASTHeader {
    ASTHEADER_CTOR_REV(ParenExpr);

	ASTHeader * expr = nullptr;
	Token op = Token();
};

// negate, --x, ++i
struct PrefixExpr : ASTHeader {
    ASTHEADER_CTOR_REV(PrefixExpr);

	Type type = Type(); // int, bool...
	Token op = Token();
};

//
// LOGIC
//

struct IfStatement : ASTHeader {
    ASTHEADER_CTOR_REV(IfStatement);

	ASTHeader * testExpr = nullptr;
	StatementSequence * body = nullptr;
	// elif pts should chain into each other if there's more than 1
	IfStatement * elifStatement = nullptr; // optional
	StatementSequence * elseSeq = nullptr; // optional
};

//
// FRONTEND
//
struct ASTTree {
	void * data;
	int allocatedSize;
	int usedSize;

	// type checking, declaration order checking
	Variable * declaredVariables[64];
	int declaredVariablesCount;

	FunctionDecl * declaredFunctions[64];
	int declaredFunctionsCount;

	ASTHeader * topLevelEntries[64];
	int topLevelEntriesCount;

	int scopeDepth;
	int declaredVariableCountThisScope[16];
};

constexpr int kMaxCompilationErrors = 10;

class Parser {
public:
    Parser (const char * fileStart, const char * filePath) 
        : tokenizer(fileStart, filePath) 
    {
        tree.data = malloc(50000);
        tree.allocatedSize = 50000;
    }

    void ParseZahFile ();

    ASTTree tree = {};

    int compilationErrorCount = 0;
    CompilationError compilationErrors[kMaxCompilationErrors] = {};

private:
    void * NewBody (ASTType type, int size);
    void InitBody (void * mem, ASTType type, int size);
    void AddDeclaredVariable (Type type, Variable * variable);
    void AddDeclaredFunction (FunctionDecl * decl);
    void PushVariableStack ();
    void PopVariableStack ();
    void CheckVariableDeclaredAndGetType (Variable * variable);
    Type GetFunctionCallType (FunctionCall * call);
    Type GetArgumentType (ASTHeader * header);

    ASTHeader * ParsePreprocessor ();
    ASTHeader * ParseMathExpr (ASTHeader * lhs, Token mathToken);
    ASTHeader * ParsePrefixExpr (Token arg);
    ASTHeader * ParseArgument (Token arg);
    ASTHeader * ParseVariableAssignment (Token identifier);
    ASTHeader * ParseVariableDeclaration (Token typeTok, Token identifierTok);
    ASTHeader * ParseFunctionCall (Token functionId);
    ASTHeader * ParseStatement ();
    IfStatement * ParseIfStatement ();
    StatementSequence * ParseStatementSequence ();
    ASTHeader * ParseFunction ();

    // TODO(james): rename these
    Token AssertGetNextToken (TokenType type);
    void AssertCurrentTokenType (TokenType type);
    void AssertTokenType (Token token, TokenType type);
    void CheckAssignmentTypeSafety (Type a, Type b, Location location);

    template <class... Args>
    void AddCompilationError (Location errorLocation, StringView msg, Args &&... args);

    template <class... Args>
    void AddCompilationError (Token token, StringView msg, Args &&... args);

    Tokenizer tokenizer; // make value after g_tokenizer is eliminated
};

//
// Backend
//
struct Compiler {
    Compiler (const ASTTree & tree) : tree(tree) {}

	int bufferSize;
	char * pos;
	int scopeDepth; // for PrintToken printing output C++

    const ASTTree & tree;
};

//
// String Formatting
//
bool format_value (
    sp::IWriter & writer, 
    const sp::StringView & format, 
    const StringView & view
) {
    sp::StringView spView(view.str, view.length);
    return sp::format_value(writer, format, spView);
}

bool format_value (
    sp::IWriter & writer, 
    const sp::StringView & format, 
    const Token & token
) {
	switch (token.type) {
		case Tok_Identifier: {
			sp::format(writer, "Token: Identifier: {}", token.content);
			break;
		}
		case Tok_String: {
			sp::format(writer, "Token: Literal: {}", token.content);
			break;
		}
		case Tok_Number: {
			sp::format(writer, "Token: Number: {}", token.content);
			break;
		}
		default: {
			sp::format(writer, "Token: {}", TokenTypeStrings[token.type]);
		}
	}

	return true;
}

void Print (StringView view) {
    std::cout.write(view.str, view.length);
}

template <class... Args>
void Print (StringView msg, Args &&... args) {
    sp::StringView view(msg.str, msg.length);
    int result = sp::print(view, std::forward<Args>(args)...);
    if (result < 0) {
        // TODO(james): fatal error 
        __debugbreak();	
    }

	// show in visual studio Output window too
	//OutputDebugString("OUTPUT WINDOW");
}

// Use this assert for assert checks on the compiler itself.
// Any malformed programs being passed to the compiler should be emitting
// Compiler Errors through the Parser.
template <class... Args>
void CompilerAssert (bool condition, StringView msg, Args &&... args) {
    if (!condition) {
        CompilerAssert(msg, std::forward<Args>(args)...);
    }
}

template <class... Args>
void CompilerAssert (StringView msg, Args &&... args) {
    sp::StringView view(msg.str, msg.length);
	char messageBuf[1024] = {};
    int result = sp::format(messageBuf, 1024, view, std::forward<Args>(args)...);
    if (result < 0) {
        // TODO(james): resize? 
        __debugbreak();	
    }

    const char * fakeFileName = "FakeFileName.cpp";
    int fakeFileNumber = 999;
    Print("Error: {}:{}: {}", fakeFileName, fakeFileNumber, messageBuf);

	getchar();
 	__debugbreak();	
}

char * CStr (StringView view) {
	char * buf = (char*)calloc(1, view.length + 1);
	memcpy(buf, view.str, view.length);
	return buf;
}

} // namespace zah
