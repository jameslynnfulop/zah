#include "zah_compiler.h"

namespace zah {

void CompileStatementSequence (Compiler * compiler, StatementSequence * seq);

template <class... Args>
void Append (Compiler * compiler, StringView format, Args &&... args) {
    sp::StringView view(format.str, format.length);
    int result = sp::format(compiler->pos, compiler->bufferSize, view, std::forward<Args>(args)...);
    if (result < 0) {
        // TODO(james): fatal error
    }

    compiler->pos += result;
    compiler->bufferSize -= result;
}

void Indent (Compiler * compiler) {
	if (compiler->scopeDepth > 0) {
		Append(compiler, "{:{}}", " ", compiler->scopeDepth * 4);
	}
}

template <class... Args>
void AppendIndented (Compiler * compiler, StringView format, Args &&... args) {
	Indent(compiler);
	Append(compiler, format, std::forward<Args>(args)...);
}

void CompileIncludePP (Compiler * compiler, IncludePP * pp) {
	if (pp->carets) {
		Append(compiler, "#include <{}>\n", pp->file);
	}
	else {
		Append(compiler, "#include \"<{}>\"\n", pp->file);
	}
}

void CompileFunctionDecl (Compiler * compiler, FunctionDecl * functionDecl) {
	AppendIndented(compiler, "{} ", functionDecl->returnType.name);
	Append(compiler, "{} (", functionDecl->name);
	if (functionDecl->argsCount > 2) {
		for (int i = 0; i < functionDecl->argsCount - 1; i++) {
			//Append(o, "{} ,");
		}
	}
	else if (functionDecl->argsCount == 1) {
		//Append(o, "{})", );
	}
	else {
		Append(compiler, ")");
	}
}

void CompileArg (Compiler * compiler, ASTHeader * header);

void CompileMathExpr (Compiler * compiler, ASTHeader * header) {
	MathExpr * mathExpr = (MathExpr*)header;
	const char * op = nullptr; // initialize to something totally invalid...
	switch (mathExpr->op.type) {
		case Tok_Plus:         op = "+";  break;
		case Tok_Minus:        op = "-";  break;
		case Tok_Star:         op = "*";  break;
		case Tok_Divide:       op = "/";  break;
		case Tok_CompEqual:    op = "=="; break;
		case Tok_CompNotEqual: op = "!="; break;
		default: CompilerAssert("Unhandled math expression");
	}
	CompileArg(compiler, mathExpr->lhs);
	Append(compiler, " {} ", op);
	CompileArg(compiler, mathExpr->rhs);

}

void CompileFunctionCall (Compiler * compiler, FunctionCall * functionCall);

void CompileArg (Compiler * compiler, ASTHeader * header) {
	switch (header->astType) {
		case AST_Variable: {
			Variable * variable = (Variable *)header;
			Append(compiler, variable->name);
			break;
		}
		case AST_StringConstant: {
			StringConstant * string = (StringConstant *)header;
			Append(compiler, "\"{}\"", string->content);
			break;
		}
		case AST_NumberConstant: {
			NumberConstant * number = (NumberConstant *)header;
			Append(compiler, "{}", number->val);
			break;
		}
		case AST_FunctionCall: {
			FunctionCall * functionCall = (FunctionCall *)header;
			CompileFunctionCall(compiler, functionCall);
			break;
		}
		case AST_MathExpr: {
			CompileMathExpr(compiler, header);
			break;
		}
		case AST_ParenExpr: {
			ParenExpr * parenExpr = (ParenExpr *)header;
			Append(compiler, "(");
			CompileArg(compiler, parenExpr->expr);
			Append(compiler, ")");
			break;
		}
		default: {
			CompilerAssert("unknown statement");
		}
	}
}

void CompileFunctionCall (Compiler * compiler, FunctionCall * functionCall) {
	Append(compiler, "{}(", functionCall->callee);

	if (functionCall->argsCount > 1) {
		for (int i = 0; i < functionCall->argsCount - 1; i++) {
			ASTHeader* header = functionCall->args[i];
			CompileArg(compiler, header);
		}
		CompilerAssert("PRINT LAST");
	}
	else if (functionCall->argsCount == 1) {
		ASTHeader* header = functionCall->args[0];
		CompileArg(compiler, header);
		Append(compiler, ")");
	}
	else {
		Append(compiler, ")");
	}

}

// x = 0;
void CompileVariableAssignment (Compiler * compiler, VariableAssignment * assign) {
	AppendIndented(compiler, "{} = ", assign->variable.name);
	CompileArg(compiler, assign->rhs);
	Append(compiler, ";\n");
}

// int x = 0;
void CompileVariableDeclarationExpression (Compiler * compiler, VariableDeclarationExpression * assign) {
	AppendIndented(compiler, "{} {} = ", assign->statement->type.name, assign->statement->variable.name);
	CompileArg(compiler, assign->rhs);
	Append(compiler, ";\n");
}

// int x;
void CompileVariableDeclarationStatement (Compiler * compiler, VariableDeclarationStatement * assign) {
	AppendIndented(compiler, "{} {};\n", assign->type.name, assign->variable.name);
}

void CompileIfStatement (Compiler * compiler, IfStatement * ifStatement) {
	Append(compiler, "if (");
	CompileArg(compiler, ifStatement->testExpr);
	Append(compiler, ")");
	CompileStatementSequence(compiler, ifStatement->body);
	if (ifStatement->elifStatement) {

		AppendIndented(compiler, "else ");
		CompileIfStatement(compiler, ifStatement->elifStatement);
		//james
	}
	if (ifStatement->elseSeq) {
		AppendIndented(compiler, "else ");
		CompileStatementSequence(compiler, ifStatement->elseSeq);
	}
}

void CompileStatementSequence (Compiler * compiler, StatementSequence * seq) {
	if (*compiler->pos == '\n') {
		AppendIndented(compiler, "{\n");
	}
	else {
		Append(compiler, "{\n");
	}
	
	compiler->scopeDepth++;
	for (int i = 0; i < seq->statementCount; ++i) {
		ASTHeader* header = seq->statements[i];
		switch (header->astType) {
			case AST_FunctionCall: {
				Indent(compiler);
				CompileFunctionCall(compiler, (FunctionCall *)header);
				Append(compiler, ";\n");
				break;
			}
			case AST_VariableAssignment: {
				CompileVariableAssignment(compiler, (VariableAssignment *)header);
				break;
			}
			case AST_VariableDeclarationExpression: {
				CompileVariableDeclarationExpression(compiler, (VariableDeclarationExpression *)header);
				break;
			}
			case AST_VariableDeclarationStatement: {
				CompileVariableDeclarationStatement(compiler, (VariableDeclarationStatement *)header);
				break;
			}
			case AST_ReturnStatement: {
				ReturnStatement * statement = (ReturnStatement *)header;
				AppendIndented(compiler, "return ");
				CompileArg(compiler, statement->rhs);
				Append(compiler, ";\n");
				break;
			}
			case AST_StatementSequence: {
				CompileStatementSequence(compiler, (StatementSequence *)header);
				break;
			}
			case AST_IfStatement: {
				Indent(compiler);
				CompileIfStatement(compiler, (IfStatement *)header);
				break;
			}
			default: {
				CompilerAssert("unhandled statement in statement sequence");
			}
		}
	}
	compiler->scopeDepth--;
	AppendIndented(compiler, "}\n");
}

void CompileFunctionDef (Compiler * compiler, FunctionDef * functionDef) {
	CompileFunctionDecl(compiler, functionDef->decl);
	CompileStatementSequence(compiler, functionDef->body);
}

void CCompiler (Compiler * compiler, char * output, int length) {
	compiler->pos = output;
	compiler->bufferSize = length;
	
	printf("Start C Transpile.\n\n");

	
	for (int i = 0; i < compiler->tree.topLevelEntriesCount; ++i) {
		ASTHeader * topLevelEntry = compiler->tree.topLevelEntries[i];

		switch (topLevelEntry->astType) {
			case AST_IncludePP: {
				printf("AST: AST_IncludePP\n");
				IncludePP* includePP = (IncludePP *)topLevelEntry;
				
				CompileIncludePP(compiler, includePP);

				break;
			}
			case AST_FunctionDef: {
				printf("AST: AST_FunctionDef\n");
				FunctionDef* functionDef = (FunctionDef *)topLevelEntry;

				CompileFunctionDef(compiler, functionDef);

				break;
			}
			default: {
				CompilerAssert("Unhandled top-level AST type");
				break;
			}
		}
	}
}

} // namespace zah
