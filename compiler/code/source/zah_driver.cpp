#include "zah_compiler.h"
#include "zah_lexer.cpp"
#include "zah_parser.cpp"
#include "zah_c_compiler.cpp"

namespace zah {

// EXPORT
static bool CompileZahBufferToBuffer (StringView zahBuffer, StringView fileName,
    char * compiledZahOutputBuffer, int32_t compiledZahOutputBufferSize
) {

	//
	// PARSING
	//
    Parser parser(zahBuffer.str, fileName.str);
	parser.ParseZahFile();

	if (parser.compilationErrorCount > 0) {
        for (int i = 0; i < parser.compilationErrorCount; ++i) {
            Print(parser.compilationErrors[i].message);
        }

		Print("Compilation failed.");
		return false;
	}


	//
	// Output
	//

	Compiler compiler(parser.tree);
    CCompiler(&compiler, compiledZahOutputBuffer, compiledZahOutputBufferSize);

    return true;
}

// EXPORT
static void CompileZahFileToFile (const char * fileInPath, const char * fileOutPath, Error * error) {
	//
	// READ FILE TO MEM
	//
	FILE * fileStream;
	errno_t fileError = fopen_s(&fileStream, fileInPath, "r");
	CompilerAssert(fileError == 0, "trouble opening input file");

	// obtain file size:
	fseek(fileStream, 0, SEEK_END);
	long fileSize = ftell(fileStream);
	rewind(fileStream);

	char * file = (char *)calloc(1, sizeof(char)*fileSize);
	fread(file, 1, fileSize, fileStream);
	fclose(fileStream);

	//
	// Output
	//

    char output[2048] = {};
    CompileZahBufferToBuffer(StringView(file, fileSize), fileInPath, &output[0], 2048);

	Print("\n COMPILED OUTPUT \n");
	Print(output);

	FILE * compiledOutput;
	fileError = fopen_s(&compiledOutput, fileOutPath, "w");
	CompilerAssert(fileError == 0, "Couldn't open output file");

	int fputResult = fputs(output, compiledOutput);
	CompilerAssert(fputResult == 0, "trouble printing to console");
	int fcloseResult = fclose(compiledOutput);
	CompilerAssert(fcloseResult == 0, "trouble closing file");
}

} // namespace zah
