#include "zah_compiler.h"
#include "zah_driver.cpp"

namespace zah {

const char* MathTest = R"(
void main () {
    int x = 1 + 2;
    int y = x - 3;
    int z = 1 - 3 + 10;

    int a = 1 - (3 + 10);
    int b = (1 - 3) + 10;

    int md = 1 * 10 / 3;

    bool t = 1 == 1;
    bool f = 1 != 1;
}
)";

const char* MathBadAssignment = R"(
void main () {
    int o = 1 - "oops"; // type mismatch // #zah_error
}
)";


void TestMain () {
    char ignoredOutputBuffer[2048] = {};
	CompileZahBufferToBuffer(
        StringView(s1, StringLength(s1)), 
        "TESTING.zah", 
        &ignoredOutputBuffer[0], 2048
    );
}

} // namespace zah


int main (int argCount, char *args[]) {
    zah::TestMain();
}
