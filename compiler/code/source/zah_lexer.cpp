namespace zah {

Token BuildToken (TokenType type, StringView view = StringView()) {
	Token t = {};
    t.content = view;
	t.type = type;

	return t;
}

void Tokenizer::IncrementSourceLineNumber () {
	this->nextTokenLineNumber++;
}

bool IsNumber (char c) {
    return (c >= '0' && c <= '9');
}

bool IsLetter (char c) {
    return (c >= 'A' && c <= 'z');
}

Token Tokenizer::PeekNextToken () {
    if (this->nextToken != Token()) {
        return this->nextToken;
    }

    const char * cPtr = this->ptr;
	Token result = BuildToken(Tok_Invalid);
	while (result.type == Tok_Invalid) {
		char c = *cPtr;
		char nextChar = *(cPtr + 1);
		switch (c) {
			case '{': {
				result = BuildToken(Tok_OpenBrace);
				break;
			}
			case '}': {
				result = BuildToken(Tok_CloseBrace);
				break;
			}
			case '(': {
				result = BuildToken(Tok_OpenParen);
				break;
			}
			case ')': {
				result = BuildToken(Tok_CloseParen);
				break;
			}
			case '<': {
				result = BuildToken(Tok_LT);
				break;
			}
			case '>': {
				result = BuildToken(Tok_GT);
				break;
			}
			case '#': {
				result = BuildToken(Tok_Hash);
				break;
			}
			case '.': {
				result = BuildToken(Tok_Period);
				break;
			}
			case ',': {
				result = BuildToken(Tok_Comma);
				break;
			}
			case '=': {
				if (nextChar == '=') {
					++cPtr;
					result = BuildToken(Tok_CompEqual);
				} else {
					result = BuildToken(Tok_Equals);
				}
				break;
			}
			case '!': {
				if (nextChar == '=') {
					++cPtr;
					result = BuildToken(Tok_CompNotEqual);
				} else {
					result = BuildToken(Tok_Negate);
				}
				break;
			}
			case '/': {
				// comment
				if (nextChar == '/') {
					while (*cPtr != '\n') {
						++cPtr;
					}
					IncrementSourceLineNumber();
				}
				// multi-line comment
				else if (nextChar == '*') {
					int multilineCommentStack = 1;

                    // skip "/*"
					cPtr += 2;

					while (multilineCommentStack > 0 && *cPtr != Tok_Eof) {
                        StringView maybeComment = StringView(cPtr, 2);

						if (StrEq(maybeComment, "/*")) {
							++multilineCommentStack;
							cPtr += StringLength("/*");
                        }
						else if (StrEq(maybeComment, "*/")) {
							--multilineCommentStack;
							cPtr += StringLength("*/");
						}
						else if (*cPtr == '\n') {
							++cPtr;
							IncrementSourceLineNumber();
						}
						else {
							++cPtr;
						}
					}
				}
				else {
					result = BuildToken(Tok_Divide);
				}
				break;
			}
			case '+': {
				result = BuildToken(Tok_Plus);
				break;
			}
			case '-': {
				result = BuildToken(Tok_Minus);
				break;
			}
			case '*': {
				result = BuildToken(Tok_Star);
				break;
			}
			case '&': {
				if (nextChar == '&') {
					result = BuildToken(Tok_LogicalAnd);
					++cPtr;
				}
				else {
					result = BuildToken(Tok_BitwiseAnd);
				}
				break;
			}
			case '|': {
				if (nextChar == '|') {
					result = BuildToken(Tok_LogicalOr);
					++cPtr;
				}
				else {
					result = BuildToken(Tok_BitwiseOr);
				}
				break;
			}
			case ' ': {
				break;
			}
			case '\r': {
				break;
			}
			case '\n': {
				IncrementSourceLineNumber();
				break;
			}
			case '\t': {
				break;
			}
			case '"': {
				int length = 0;
				const char * start = 0;
				++cPtr;
				while (*cPtr != '"') {
					if (start == 0) start = cPtr;
					++cPtr;
					++length;
				}
				result = BuildToken(Tok_String, StringView(start, length));
				break;
			}
			case ';': {
				result = BuildToken(Tok_Semicolon);
				break;
			}
			case '\0': {
				result = BuildToken(Tok_Eof);
				break;
			}
			default: {
				if (IsLetter(c)) {
					int length = 0;
					const char * start = cPtr;
					while (IsLetter(c) || IsNumber(c)) {
						length++;
						c = cPtr[length];
					}
					cPtr += length - 1;
					
					StringView string(start, length);
					if (StrEq(string, StringView("return"))) {
						result = BuildToken(Tok_Return);
					}
					else if (StrEq(string, StringView("if"))) {
						result = BuildToken(Tok_If);
					}
					else if (StrEq(string, StringView("else"))) {
						result = BuildToken(Tok_Else);
					}
					else if (StrEq(string, StringView("switch"))) {
						result = BuildToken(Tok_Switch);
					}
					else if (StrEq(string, StringView("case"))) {
						result = BuildToken(Tok_Case);
					}
					else if (StrEq(string, StringView("true"))) {
						result = BuildToken(Tok_True);
					}
					else if (StrEq(string, StringView("false"))) {
						result = BuildToken(Tok_False);
					}
					else {
						result = BuildToken(Tok_Identifier, StringView(start, length));
					}
				}
				// Is it a number constant?
				else if (IsNumber(c) || c == '-' || c == '+') {
					int length = 0;
					const char * start = cPtr;
					while (IsNumber(c) || c == '.' ) {
						length++;
						c = cPtr[length];
					}
					cPtr += length - 1;
					result = BuildToken(Tok_Number, StringView(start, length));
				}
				else {
					result = BuildToken(Tok_Unknown);
				}

				break;
			}
		}

		++cPtr;
	}


	this->nextToken = result;
	this->nextTokenPtr = cPtr;
    this->nextToken.location.file = file;
    this->nextToken.location.lineNumber = nextTokenLineNumber;

	return result;
}

Token Tokenizer::GetNextToken () {
    if (this->nextToken != Token()) {
        // next --> current
        this->currentToken = this->nextToken;
        this->lineNumber = this->nextTokenLineNumber;

        // move ptr forward
        this->ptr = this->nextTokenPtr;

        // clear next
        this->nextToken = Token();

        Print("{}\n", this->currentToken);

        return this->currentToken;
    }
    else {
        PeekNextToken();
        return GetNextToken();
    }
}

Token Tokenizer::GetCurrentToken () const {
	return this->currentToken;
}


bool IsPrefixOperator (Token tok) {
	return (
		tok.type == Tok_Negate
	);
}

bool IsMathOperator (Token tok) {
	return (
		tok.type == Tok_Plus || 
		tok.type == Tok_Minus || 
		tok.type == Tok_Divide || 
		tok.type == Tok_Star || 
		tok.type == Tok_CompEqual || 
		tok.type == Tok_CompNotEqual ||
		tok.type == Tok_LogicalAnd ||
		tok.type == Tok_BitwiseAnd ||
		tok.type == Tok_LogicalOr ||
		tok.type == Tok_BitwiseOr
	);
}

} // namespace zah
